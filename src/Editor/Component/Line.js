import React, { Component } from 'react';
import ContentEditable from 'react-contenteditable';
import DeleteLineButton from './Buttons/DeleteLineButton';
import ChangeLineTypeButton from './Buttons/ChangeLineTypeButton';
import { connect } from 'react-redux';
import { newLine, deleteLine, editLine, newLineAfter, addPage, moveLine, newPageAfter, deletePage, addRef, shiftLines, changePageId } from '../actions';
import { getCaretCharacterOffsetWithin, linesPerPage } from '../helpers';

class Line extends Component {

	componentDidMount() {
		let { addRef, linesCount, lineRefs } = this.props;
		if (!lineRefs[linesCount]) {
			addRef(React.createRef(), linesCount)
		}	
	}

	focusLine = (count) => {
		let { linesCount, listRef, lineRefs } = this.props;
		setTimeout(() => {
			if (typeof lineRefs[linesCount + count] !== 'undefined' && lineRefs[linesCount + count].current !== null) {
				lineRefs[linesCount + count].current.el.current.focus()
			}
		}, 0) 
	}
	
	onEdit = (e) => {
		let { id, pageId, lines, editLine, index, pageIndex } = this.props
		let { keyCode, target } = e;
		if((keyCode < 37 || keyCode > 40) && keyCode !== 13) {
			editLine(pageIndex, index, target.innerHTML);
			let text = '';
			lines.map((line) => {
				return text + line.content;
			})
		} else if(keyCode === 40) {
			this.focusLine(1)
		} else if (keyCode === 38) {
			this.focusLine(-1)
		}
	}

	onDown = (e) => {
		let { pageId, newLine, newLineAfter, editLine, lines, index, id, lineType, newPageAfter, moveLine, content, deleteLine, deletePage, pages, pageIndex } = this.props;
		let startPosition = getCaretCharacterOffsetWithin(e.target);
		let selection = window.getSelection();
		let html = e.target.innerHTML;
		if (e.keyCode === 13) {
			if (!e) e = window.event;
			if (e.preventDefault) e.preventDefault();
			else e.returnValue = false;

			if (selection.focusOffset !== selection.anchorOffset) { /** the user has selected sm */
				if(lines.length < linesPerPage) {
					if(lines.length === (index + 1) && (selection.anchorOffset - selection.focusOffset) === content.length) {
						editLine(pageIndex, index, '');
						newLine(pageIndex);
						this.focusLine(1)
					}
					else {
						let txt = html.replace(/&nbsp;/g, ' ');
						editLine(pageIndex, index, txt.slice(0, selection.focusOffset));
						newLineAfter(pageIndex, index, lineType, txt.slice(selection.anchorOffset, txt.length));
						this.focusLine(1)
					}
				}
				else {
					if (index === 17){
						if ((selection.anchorOffset - selection.focusOffset) === content.length) {
							editLine(pageIndex, index, '');
							newPageAfter(pageIndex);
							this.focusLine(1)
						}
						else {
							newPageAfter(pageIndex)
							let txt = html.replace(/&nbsp;/g, ' ');
							editLine(pageIndex, index, txt.slice(0, selection.focusOffset));
							newLineAfter(pageIndex, index, lineType, txt.slice(selection.anchorOffset, txt.length));
							moveLine(pageIndex, index);
							this.focusLine(1)
						}
					}
					else {
						if ((selection.anchorOffset - selection.focusOffset) === content.length) {
							editLine(pageIndex, index, '');
							newLineAfter(pageIndex, index, 'p', '');
							newPageAfter(pageIndex);
							moveLine(pageIndex, index)
							this.focusLine(1)
						}
						else {
							newPageAfter(pageIndex)
							let txt = html.replace(/&nbsp;/g, ' ');
							editLine(pageIndex, index, txt.slice(0, selection.focusOffset));
							newLineAfter(pageIndex, index, lineType, txt.slice(selection.anchorOffset, html.length));
							moveLine(pageIndex, index);
							this.focusLine(1)
						}
					}
					
				}
			}
			else { /** the user has not selected anything */
				if(lines.length < linesPerPage) {
					if(lines.length === (index + 1) && startPosition === html.length) {
						newLine(pageIndex);
						this.focusLine(1)
					}
					else {
						let txt = html.replace(/&nbsp;/g, ' ');
						editLine(pageIndex, index, txt.slice(0, startPosition));
						newLineAfter(pageIndex, index, lineType, txt.slice(startPosition, html.length));
						this.focusLine(1)
					}
				}
				else {
					if (index === 17) {
						if (startPosition === html.length) { 
							newPageAfter(pageIndex);
							this.focusLine(1)
						}
						else {
							newPageAfter(pageIndex)
							let txt = html.replace(/&nbsp;/g, ' ');
							editLine(pageIndex, index, txt.slice(0, startPosition));
							newLineAfter(pageIndex, index, 'p', txt.slice(startPosition, html.length));
							moveLine(pageIndex, index);
							this.focusLine(1)
						}
					}
					else {
						if (startPosition === html.length) {
							newLineAfter(pageIndex, index, 'p', '');
							this.focusLine(1)
							if (pages.length === pageIndex + 1 || pages[pageIndex + 1].content.length >= linesPerPage) {
								newPageAfter(pageIndex);
								moveLine(pageIndex, index);
								this.focusLine(1)
							} else {
								moveLine(pageIndex, index);
								this.focusLine(1)
							}
						}
						else if (pages.length === pageIndex + 1 || pages[pageIndex + 1].content.length >= linesPerPage) {
							newPageAfter(pageIndex)
							let txt = html.replace(/&nbsp;/g, ' ');
							editLine(pageIndex, index, txt.slice(0, startPosition));
							newLineAfter(pageIndex, index, 'p', txt.slice(startPosition, html.length));
							moveLine(pageIndex, index);
							this.focusLine(1)
						} else {
							let txt = html.replace(/&nbsp;/g, ' ');
							editLine(pageIndex, index, txt.slice(0, startPosition));
							newLineAfter(pageIndex, index, 'p', txt.slice(startPosition, html.length));
							moveLine(pageIndex, index);
							this.focusLine(1)
						}
					}
					
				}
			}
		}
	
		if (e.keyCode === 8 && content === '') {
			deleteLine(pageIndex, index)
			if(lines.length === 0) {
				deletePage(pageIndex)
			}	
			this.focusLine(-1)
		}
	}

	onClip = (e) => {
		let { pageId, id, editLine, shiftLines, changePageId, index, pageIndex } = this.props;
		let sel = window.getSelection();
		let { focusOffset, anchorOffset } = sel
		let target = e.target
		let html = target.innerHTML;
		let pastedText = e.clipboardData.getData('Text');
		let js = pastedText.replace(/\n/g, '<br>');
		let pastedLines = js.split("<br>");
		
		e.persist();
		if (!e) e = window.event;
		if (e.preventDefault) e.preventDefault();
		else e.returnValue = false;

		let txt = html.replace(/&nbsp;/g, ' ');
		if (focusOffset !== anchorOffset) { /** if the user select some char */
			txt = txt.replace(txt.substring(anchorOffset, focusOffset), "")
			if (anchorOffset < focusOffset)
				focusOffset = anchorOffset
		}
		pastedLines = [...pastedLines, txt.slice(focusOffset)]
		editLine(pageIndex, index, txt.slice(0, focusOffset) + pastedLines[0])
		pastedLines.shift()
		let focusIndex = pastedLines.length
		shiftLines(pageIndex, index, pastedLines)
		changePageId()
		this.focusLine(focusIndex)
	}

	render(){
		let { id, pageId, lineType, content, lineRefs, linesCount, pageIndex, index, lines } = this.props;
		return(
			<div className="line-container animated slideInDown">
				<DeleteLineButton pageIndex={pageIndex} lineIndex={index} lines={lines} />
					<ContentEditable id={id} ref={lineRefs[linesCount]} className={'editable-content ' + id} tagName={lineType} onPaste={this.onClip} html={content} onKeyDown={this.onDown} onKeyUp={this.onEdit}/>
				<ChangeLineTypeButton lineIndex={index} pageIndex={pageIndex}/>
			</div>
		)
	}
};

const mapStateToProps = (state, ownProps) => ({
    content : ownProps.content,
    lineType : ownProps.lineType,
    index : ownProps.index,
	pages: state.pageReducer,
	lineRefs: state.refReducer
});

const mapdispatchtoprops = (dispatch) => ({
    newLine: (pageId) => dispatch(newLine(pageId)),
    deleteLine: (lineId, pageId) => dispatch(deleteLine(lineId, pageId)),
    editLine: (pageIndex, lineIndex, text) => dispatch(editLine(pageIndex, lineIndex, text)),
	newLineAfter: (pageIndex, lineIndex, lineType, text) => dispatch(newLineAfter(pageIndex, lineIndex, lineType, text)),
	addPage: () => dispatch(addPage()),
	moveLine: (pageIndex, lineIndex) => dispatch(moveLine(pageIndex, lineIndex)),
	newPageAfter: (pageIndex) => dispatch(newPageAfter(pageIndex)),
	deletePage: (index) => dispatch(deletePage(index)),
	addRef: (ref, index) => dispatch(addRef(ref, index)),
	shiftLines: (pageIndex, lineIndex, pastedLines) => dispatch(shiftLines(pageIndex, lineIndex, pastedLines)),
	changePageId: () => dispatch(changePageId())
});

export default connect(mapStateToProps, mapdispatchtoprops)(Line);