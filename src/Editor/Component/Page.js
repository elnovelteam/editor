import React, { Component } from 'react';
import { connect } from 'react-redux';
import { newLine, deleteLine, deletePage } from '../actions';
import Line from './Line';
import DeletePageButton from './Buttons/DeletePageButton';

class Page extends Component {
	constructor(props) {
		super(props)
	}

	componentDidUpdate = () => {
		this.props.measure()
	}

	createFirstLine = () => {
		let { lines, id, deletePage } = this.props;
		if(lines.length === 0) {
			deletePage(id)
		}
	}

	countLines = () => {
		let { index, pages } = this.props;
		this.linesCount = 0;
		pages.map((page, pageIndex) => {
			if (pageIndex < index) {
				page.content.map((line, lineIndex) => {
					this.linesCount += 1
					return line
				})
			}
			return page
		})
	}

	render() {
		let { lines, id, index, listRef } = this.props;
		this.countLines()
		return (
			<div className='page col-lg-8 col-sm-10 animated fadeIn' onClick={this.createFirstLine}>
				<DeletePageButton index={index}/>
				{lines.map((line, i) => {
					return (
						<Line {...line} key={i} index={i} pageId={id} pageIndex={index} lines={lines} linesCount={this.linesCount + i} listRef={listRef} />
					)
				})}
				<p>(( { index + 1 } ))</p>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	lines: ownProps.content,
	pages: state.pageReducer,
});

const mapDispatchToProps = dispatch => ({
	newLine: pageIndex => dispatch(newLine(pageIndex)),
	deleteLine: (pageIndex, lineIndex) => dispatch(deleteLine(pageIndex, lineIndex)),
	deletePage: (index) => dispatch(deletePage(index)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Page);