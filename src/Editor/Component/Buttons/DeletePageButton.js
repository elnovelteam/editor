import React, { Component } from 'react';
import { deletePage } from '../../actions';
import { connect } from 'react-redux';

class DeletePageButton extends Component {
    render() {
        let { index, deletePage } = this.props;
        return (
            <button onClick={() => deletePage(index)}>
                <i className="fa fa-trash fa-2x"></i>
            </button>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    index: ownProps.index
})

const mapDispatchToProps = (dispatch) => ({
    deletePage: (index) => dispatch(deletePage(index))
})

export default connect(mapStateToProps, mapDispatchToProps)(DeletePageButton);