import React, { Component } from 'react';
import { connect } from 'react-redux';
import { newLine, newPageAfter } from '../../actions'
import { linesPerPage } from '../../helpers';

class NewLineButton extends Component {
    
    handleNewLine = () => {
        let {pageId, newLine, lines, newPageAfter, pageIndex} = this.props;
        if (lines.length < linesPerPage)
            newLine(pageIndex)
        else
            newPageAfter(pageId);
    }
    
    render() {
        return (
            <div onClick={ this.handleNewLine }>
                <button type='button'>New Line</button>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    pageId: ownProps.pageId
})
const mapDispatchToProps = (dispatch) => ({
    newLine: (pageIndex) => dispatch(newLine(pageIndex)),
    newPageAfter: (pageId) => dispatch(newPageAfter(pageId))
})

export default connect(mapStateToProps, mapDispatchToProps)(NewLineButton);