import React, { Component } from 'react'

export default class NewPageButton extends Component {
	render() {
		let {onClick} = this.props;
		return (
			<button onClick={onClick}>
				<i className="fa fa-plus-square fa-2x"></i>
			</button>
		)
	}
}
