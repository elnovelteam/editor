import React, { Component } from 'react';
import { changeLineType } from '../../actions';
import { connect } from 'react-redux';

class ChangeLineType extends Component {
    render() {
        let { lineIndex, changeLineType, pageIndex } = this.props;
        return (
        // <button className="change-line-type" onClick={() => changeLineType(id, pageId, lineType)}>
        //     <i className="fa fa-ellipsis-v fa-2x"></i>
        // </button>
        <div className="dropdown">
            <button className="dropbtn"><i className="fa fa-ellipsis-v fa-2x"></i></button>
            <div className="dropdown-content animated fadeIn">
                <h1 onClick={() => changeLineType(pageIndex, lineIndex, 'h1')}>نمونه متن</h1>
                <h2 onClick={() => changeLineType(pageIndex, lineIndex, 'h2')}>نمونه متن</h2>
                <h3 onClick={() => changeLineType(pageIndex, lineIndex, 'h3')}>نمونه متن</h3>
                <h4 onClick={() => changeLineType(pageIndex, lineIndex, 'h4')}>نمونه متن</h4>
                <h5 onClick={() => changeLineType(pageIndex, lineIndex, 'h5')}>نمونه متن</h5>
                <h6 onClick={() => changeLineType(pageIndex, lineIndex, 'h6')}>نمونه متن</h6>
                <p onClick={() => changeLineType(pageIndex, lineIndex, 'p')}>پاراگراف</p>
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    
})
const mapDispatchToProps = (dispatch) => ({
    changeLineType: (pageIndex, lineIndex, lineType) => dispatch(changeLineType(pageIndex, lineIndex, lineType))
})

export default connect(mapStateToProps, mapDispatchToProps)(ChangeLineType);