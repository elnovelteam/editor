import React, { Component } from 'react';
import { deleteLine, deletePage } from '../../actions';
import { connect } from 'react-redux';

class DeleteLineButton extends Component {

    handleDelete = () => {
        let { deleteLine, deletePage, lines, pageIndex, lineIndex } = this.props;
        deleteLine(pageIndex, lineIndex)
        if(lines.length === 0) {
            deletePage(pageIndex)
        }	
    }

    render() {
        return (
            <button className="delete-line" onClick={this.handleDelete.bind(this)}>
                <i className="fa fa-times fa-2x"></i>
            </button>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    pageIndex: ownProps.pageIndex,
    lineIndex: ownProps.lineIndex
})
const mapDispatchToProps = (dispatch) => ({
    deleteLine: (pageIndex, lineIndex) => dispatch(deleteLine(pageIndex, lineIndex)),
    deletePage: (index) => dispatch(deletePage(index)),
})

export default connect(mapStateToProps, mapDispatchToProps)(DeleteLineButton);