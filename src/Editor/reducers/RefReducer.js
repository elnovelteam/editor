import { ADD_REF } from '../actions/actionTypes';

const refReducer = (state=[], action) => {
    switch(action.type) {
        case ADD_REF:
            return Object.assign({}, state, {
                [action.index]: action.ref
            })

        default:
            return state
    }
}

export default refReducer;