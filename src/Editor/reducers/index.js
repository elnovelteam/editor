import { combineReducers } from "redux";
import pageReducer from './PageReducer';
import refReducer from './RefReducer';

export default combineReducers({
    pageReducer,
    refReducer
});