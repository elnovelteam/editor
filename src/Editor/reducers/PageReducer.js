import { ADD_PAGE, DELETE_PAGE, NEW_LINE, DELETE_LINE, EDIT_LINE, NEW_LINE_AFTER, CHANGE_LINE_TYPE, MOVE_LINE, NEW_PAGE_AFTER, SHIFT_LINES,CHANGE_PAGE_ID } from "../actions/actionTypes";
import { linesPerPage } from '../helpers';

/** here we make a copy of the state and perform our changes on it in each case, then return it (redux suggest that never make changes to the original state) */
var stateCopy, pageIndex, lineIndex, pageContent, lineContent, lineType, newPage, newLine 

/** we make a new line here */
var makeLine = (content='', lineType='p', id=Date.now()) => {
	newLine = {id: id, content: content, lineType: lineType}
}

/** we make a new page here */
var makePage = (id=Date.now(), content= [newLine]) => {
	newPage = {id: id, content: content} /** remember to add the cc: 0 later */
}

/** we use this func to make a copy of our originakl state and make changes on it */
var makeStateCopy = (state) => {
	stateCopy = Object.values(Object.assign({}, state))
}

/** we use these func to set our variables such as indexes of the line and page targets */
var setVariables = (state, action) => {
	makeStateCopy(state)
	/** this code supposed to set the variables dynamically but it doesn't work(type \ or ' in a line and see the problem) */
	// Object.keys(action).map((var_name, i) => {
	// 	if (var_name !== 'type' && var_name !== 'pastedLines') {
	// 		let newValue = action[var_name]
	// 		typeof newValue === "string" ? eval(var_name + " = '" + newValue + "'") : eval(var_name + " = " + newValue);
	// 		if (var_name === 'lineContent') {
	// 			lineContent = lineContent.replace(/&nbsp;/g, ' ')
	// 		}
	// 	}
	// 	return var_name
	// })
	typeof action["pageIndex"] !== "undefined" ? pageIndex = action["pageIndex"] : pageIndex = pageIndex
	typeof action["lineIndex"] !== "undefined" ? lineIndex = action["lineIndex"] : lineIndex = lineIndex
	typeof action["lineContent"] !== "undefined" ? lineContent = action["lineContent"].replace(/&nbsp;/g, ' ') : lineContent = lineContent
	typeof action["lineType"] !== "undefined" ? lineType = action["lineType"] : lineType = lineType
	pageContent = stateCopy[pageIndex].content
	/** in case we wanted to use this fucn and there was no page index passed (currently we don't have such case in our reducer) */
	// typeof pageIndex !== "undefined" ? pageContent = stateCopy[pageIndex].content : pageContent = pageContent
}

const pageReducer = (state=[], action) => {
	switch (action.type) {
        case ADD_PAGE:
			makeStateCopy(stateCopy)
			makeLine()
			makePage()
			stateCopy.push(newPage)
			return stateCopy

        case DELETE_PAGE:
			setVariables(state, action)
			stateCopy.splice(pageIndex, 1)
			return stateCopy

		case NEW_LINE:
			setVariables(state, action)
			makeLine()
			pageContent.push(newLine)
			return stateCopy;

		case DELETE_LINE:
			setVariables(state, action)
			pageContent.splice(lineIndex, 1)
			return stateCopy

		case EDIT_LINE:
			setVariables(state, action)
			pageContent[lineIndex].content = lineContent
			return stateCopy

		case NEW_LINE_AFTER:
			setVariables(state, action)
			makeLine(lineContent, lineType)
			pageContent.splice(lineIndex + 1, 0, newLine)
			return stateCopy;

		case CHANGE_LINE_TYPE:
			setVariables(state, action)
			pageContent[lineIndex].lineType = lineType
			return stateCopy

		case MOVE_LINE:
			setVariables(state, action)
			let movedLine = pageContent.splice(pageContent.length-1)
			stateCopy[pageIndex + 1].content = [movedLine[0], ...stateCopy[pageIndex + 1].content]
			return stateCopy

		case SHIFT_LINES:
			setVariables(state, action)
			let {pastedLines} = action
			let maxInsertCountInFirstPage = linesPerPage - (lineIndex + 1); /** the max number of lines which can be inserted in the first page */
			let insertedCountInFirstPage = pastedLines.length > maxInsertCountInFirstPage ? maxInsertCountInFirstPage : pastedLines.length; /** this is the count of lines which will be inserted in the first page */  
			let insertedLinesInFirstPage = pastedLines.splice(0, insertedCountInFirstPage) /** the lines inserted in the first page */
			let shiftedFromFirstPage = []; /** the lines that will be in a new page after paste some content */
			for(let j = insertedCountInFirstPage - 1; j >= 0 ; j--) {
				makeLine(insertedLinesInFirstPage[j], 'p')
				pageContent.splice((lineIndex + 1), 0, newLine)
			}

			if (pageContent.length > linesPerPage) { /** if the content of the page would be more than linesPerPage this condition will pull out the extra lines in the last of the page */
				while(pageContent.length > linesPerPage) {
					shiftedFromFirstPage.unshift(pageContent[pageContent.length - 1])
					pageContent.pop()
				}
			}

			while (pastedLines.length >= linesPerPage) { /** here we will make and fill the pages between pasted position and the content after it */
				let pageContent = pastedLines.splice(0, linesPerPage)
				pageContent.map((line, i) => {
					makeLine(line, 'p', `${Date.now()}${i}`)
					pageContent[i] = newLine
					return line
				})
				makePage(`${Date.now()}${pageIndex}`, pageContent)
				stateCopy.splice((pageIndex + 1), 0, newPage)
				pageIndex ++
			}

			if (pastedLines.length > 0) { /** here we will take care of the last pasted lines that are lower than linesPerPage and try to combine them with the shifted lines from the first page */
				pageContent = pastedLines.splice(0, pastedLines.length) /** last pasted contents */
				pageContent.map((line, i) => {
					makeLine(line, 'p', `${Date.now()}${i}`)
					pageContent[i] = newLine
					return line
				})
				if (pastedLines.length + shiftedFromFirstPage.length <= linesPerPage) { /** if the last pasted lines and shifted lines were filled in one page we will combine them */
					makePage(`${Date.now()}${pageIndex}`, [...pageContent, ...shiftedFromFirstPage])
					stateCopy.splice((pageIndex + 1), 0, newPage)
				} else { /** otherwise we make to pages and fill the first one with linesPerPage lines */
					makePage(`${Date.now()}${pageIndex}`, pageContent)
					stateCopy.splice((pageIndex + 1), 0, newPage)
					while(stateCopy[pageIndex].length < linesPerPage) { /** now we will fill the page with shifted lines form the first page till it's full */
						stateCopy[pageIndex].content.push(shiftedFromFirstPage.shift())
					}
					pageIndex ++
					if (shiftedFromFirstPage.length > 0) { /** and then if there is any shifted line left we put it in this new page */
						makePage(`${Date.now()}${pageIndex}`, shiftedFromFirstPage)
						stateCopy.splice((pageIndex + 1), 0, newPage)
					}
				}
			}
			return stateCopy

			case NEW_PAGE_AFTER:
				setVariables(state, action)
				makeLine()
				makePage()
				stateCopy.splice((pageIndex + 1), 0, newPage)
				return stateCopy

			case CHANGE_PAGE_ID:
				makeStateCopy(state)
				stateCopy.map((page, i) => {
					stateCopy[i].id += 2
					return page
				})
				return stateCopy

        	default:
            	return state
    }
}

export default pageReducer;