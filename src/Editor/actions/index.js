import { 
	ADD_PAGE,
	DELETE_PAGE, 
	NEW_LINE, 
	DELETE_LINE, 
	EDIT_LINE, 
	NEW_LINE_AFTER, 
	CHANGE_LINE_TYPE, 
	MOVE_LINE, 
	NEW_PAGE_AFTER, 
	ADD_REF,
	SHIFT_LINES,
	CHANGE_PAGE_ID
} from "./actionTypes";

/** PageReducer */
export const addPage = () => ({
	type: ADD_PAGE
});

export const deletePage = index => ({
	type: DELETE_PAGE,
	pageIndex: index 
});

export const newLine = index => ({
	type: NEW_LINE,
	pageIndex: index
});

export const deleteLine = (pageIndex, lineIndex) => ({
	type: DELETE_LINE,
	pageIndex: pageIndex,
	lineIndex: lineIndex
});

export const editLine = (pageIndex, lineIndex, lineContent) => ({
	type: EDIT_LINE,
	pageIndex: pageIndex,
	lineIndex: lineIndex,
	lineContent: lineContent
});

export const newLineAfter = (pageIndex, lineIndex, lineType, lineContent) => ({
	type: NEW_LINE_AFTER,
	pageIndex: pageIndex,
	lineIndex: lineIndex,
	lineType: lineType,
	lineContent: lineContent
});

export const changeLineType = (pageIndex, lineIndex, lineType) => ({
	type: CHANGE_LINE_TYPE,
	pageIndex: pageIndex,
	lineIndex: lineIndex,
	lineType: lineType
});

export const moveLine = (pageIndex, lineIndex) => ({
	type: MOVE_LINE,
	pageIndex: pageIndex,
	lineIndex: lineIndex,
});

export const newPageAfter = (pageIndex) => ({
	type: NEW_PAGE_AFTER,
	pageIndex: pageIndex
});

export const shiftLines = (pageIndex, lineIndex, pastedLines) => ({
	type: SHIFT_LINES,
	pageIndex: pageIndex,
	lineIndex: lineIndex,
	pastedLines: pastedLines
})

export const changePageId = () => ({
	type: CHANGE_PAGE_ID
})

/** RefReducer */
export const addRef = (ref, index) => ({
	type: ADD_REF,
	ref: ref,
	index: index
})