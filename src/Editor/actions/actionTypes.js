/** Page Reducer */
export const ADD_PAGE = 'ADD_PAGE';
export const DELETE_PAGE = 'DELETE_PAGE';
export const NEW_LINE = 'NEW_LINE';
export const DELETE_LINE = 'DELETE_LINE';
export const EDIT_LINE = 'EDIT_LINE';
export const NEW_LINE_AFTER = 'NEW_LINE_AFTER';
export const CHANGE_LINE_TYPE = 'CHANGE_LINE_TYPE';
export const MOVE_LINE = 'MOVE_LINE';
export const NEW_PAGE_AFTER = 'NEW_PAGE_AFTER';
export const SHIFT_LINES = 'SHIFT_LINES';
export const CHANGE_PAGE_ID = 'CHANGE_PAGE_ID'

/** Ref Reducer */
export const ADD_REF = 'ADD_REF';