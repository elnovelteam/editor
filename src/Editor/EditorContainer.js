import React, { Component } from 'react'
import {Provider} from 'react-redux';
import { createStore } from "redux";
import reducers from './reducers';
import Editor from './Editor';

export default class EditorContainer extends Component {
    
    store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    render() {
        return (
            <Provider store={this.store}>
                <Editor/>
            </Provider>
        )
    }
}