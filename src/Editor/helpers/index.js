export const getCaretCharacterOffsetWithin = (e) => {
	var caretOffset = 0;
	var doc = e.ownerDocument || e.document;
	var win = doc.defaultView || doc.parentWindow;
	var sel;
	if (typeof win.getSelection != "undefined") {
	sel = win.getSelection();
	if (sel.rangeCount > 0) {
		var range = win.getSelection().getRangeAt(0);
		var preCaretRange = range.cloneRange();
		preCaretRange.selectNodeContents(e);
		preCaretRange.setEnd(range.endContainer, range.endOffset);
		caretOffset = preCaretRange.toString().length;
	}
	} else if ((sel = doc.selection) && sel.type !== "Control") {
	var textRange = sel.createRange();
	var preCaretTextRange = doc.body.createTextRange();
	preCaretTextRange.moveToElementText(e);
	preCaretTextRange.setEndPoint("EndToEnd", textRange);
	caretOffset = preCaretTextRange.text.length;
	}
	return caretOffset;
};

export const WordCount = (str) => {
		return str.split(" ").length;
};

  
export const insertTextAtCursor = (text) => {
    var sel, range;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode( document.createTextNode(text) );
        }
    } else if (document.selection && document.selection.createRange) {
        document.selection.createRange().text = text;
    }
};

export const linesPerPage = 10;