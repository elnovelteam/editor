import React, {Component} from 'react';
import {connect} from 'react-redux';
import { addPage, deletePage } from "./actions";
import Page from './Component/Page';
import NewPageButton from './Component/Buttons/NewPageButton';
import 'react-virtualized/styles.css';
import { List, WindowScroller, CellMeasurerCache, CellMeasurer } from 'react-virtualized';

class Editor extends Component{
	constructor(props) {
		super(props)
		this.listRef = React.createRef()
		this.cache = new CellMeasurerCache({
			fixedWidth: true,
			minHeight: 1250,
			defaultHeight: 1250
		})
	}

	saveData = () => {
		/** send data to the server */
	}
	
    render() {
		let {pages, addPage} = this.props;
		let rowRenderer = ({
			key, // Unique key within array of rows
			index, // Index of row within collection
			isScrolling, // The List is currently being scrolled
			isVisible, // This row is visible within the List (eg it is not an overscanned row)
			style,
			parent
		}) => {
			return (
				<CellMeasurer
					key={key}
					cache={this.cache}
					columnIndex={0}
					rowIndex={index}
					parent={parent}
				>
					{({measure, registerChild}) => {
					return (
						<div ref={registerChild} style={style} key={key}>
							<Page {...pages[index]} measure={measure} key={index} index={index} listRef={this.listRef} />
						</div>
					)}}
				</CellMeasurer>
			)
		}

        return (
            <div className="col-12 editor-container">
				<div className="col-12 top-fixed-box">
					{pages.length >= 1 &&
						<button className="animated rollIn" onClick={this.saveData}><i className="fa fa-save fa-2x"></i></button>
					}
					<NewPageButton onClick={() => addPage()}/>
				</div>
				<WindowScroller scrollElement={document.body}>
					{({ height, isScrolling, onChildScroll, scrollTop }) => (
						<List
							ref={this.listRef}
							autoHeight
							height={height}
							width={1}
							rowCount={pages.length}
							rowRenderer={rowRenderer}
							deferredMeasurementCache={this.cache}
							rowHeight={this.cache.rowHeight}
							isScrolling={isScrolling}
							onScroll={onChildScroll}
							scrollTop={scrollTop}
							className="col-12"
							containerStyle={{width: "100%", maxWidth: "100%"}}
							style={{width: "100%"}}
						/>
					)}
				</WindowScroller>
            </div>
        )
    }
}

const mapStateToProps = state => ({
	pages: state.pageReducer
});

const mapDispatchToProps = dispatch => ({ 
	addPage: () => dispatch(addPage()),
	deletePage: pageIndex => dispatch(deletePage(pageIndex))
});

export default connect(mapStateToProps, mapDispatchToProps)(Editor);
